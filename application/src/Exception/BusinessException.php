<?php

namespace App\Exception;

use Throwable;

class BusinessException extends \Exception
{
    public function __construct($message = "", $statusCode = 400)
    {
        parent::__construct($message, $statusCode);
    }
}
