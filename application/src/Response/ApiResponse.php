<?php

namespace App\Response;

abstract class ApiResponse
{
    /**
     * @var string
     */
    private string $status;

    /**
     * @var int
     */
    private int $statusCode;

    /**
     * @var ?string
     */
    private ?string $message = '';

    /**
     * @param $status
     * @param $statusCode
     */
    protected function __construct($status, $statusCode)
    {
        $this->status = $status;
        $this->statusCode = $statusCode;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatusCode(): ?int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     *
     * @return $this
     */
    public function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     *
     * @return $this
     */
    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param $array
     *
     * @return mixed
     */
    public function setData($array){

    }
}
