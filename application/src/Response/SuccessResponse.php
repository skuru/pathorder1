<?php

namespace App\Response;

use App\Enum\StatusTextType;

class SuccessResponse extends ApiResponse
{
    /**
     * @var mixed
     */
    private $data;

    /**
     * @var string|string[]
     */
    private $groups;

    /**
     * @param string $status
     */
    protected function __construct(string $status = StatusTextType::SUCCESS)
    {
        parent::__construct($status, 200);
    }

    /**
     * @param string $status
     *
     * @return static
     */
    public static function create(
        string $status = StatusTextType::SUCCESS
    ): self {
        return new static($status);
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     *
     * @return SuccessResponse
     */
    public function setData($data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return string|string[]
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param string|string[] $groups
     *
     * @return SuccessResponse
     */
    public function setGroups($groups): self
    {
        $this->groups = $groups;

        return $this;
    }
}
