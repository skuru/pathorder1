<?php

namespace App\Response;

use App\Enum\StatusTextType;

class ErrorResponse extends ApiResponse
{
    /**
     * @var mixed
     */
    private $errors;

    /**
     * @param string $status
     * @param int $statusCode
     */
    protected function __construct(string $status = StatusTextType::ERROR, int $statusCode = 400)
    {
        parent::__construct($status, $statusCode);
    }

    /**
     * @param string $status
     * @param int $statusCode
     *
     * @return static
     */
    public static function create(string $status = StatusTextType::ERROR, int $statusCode = 400): self
    {
        return new static($status, $statusCode);
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     *
     * @return ErrorResponse
     */
    public function setErrors($errors): ErrorResponse
    {
        $this->errors = $errors;

        return $this;
    }
}
