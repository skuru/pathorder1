<?php

namespace App\Request;

use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Optional;

class OrderUpdateRequest extends ApiRequest
{
    public function rules(): array
    {
        return [
            'quantity' => new Optional([
                new GreaterThan(0)
            ]),
            'address' => new Optional([
                new NotBlank(),
                new NotNull()
            ]),
        ];
    }
}
