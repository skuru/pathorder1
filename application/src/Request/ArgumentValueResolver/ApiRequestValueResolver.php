<?php

namespace App\Request\ArgumentValueResolver;

use App\Request\ApiRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class ApiRequestValueResolver implements ArgumentValueResolverInterface
{
    private ValidatorInterface $validator;

    /**
     * ApiRequestValueResolver constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @inheritDoc
     * @throws \ReflectionException
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        if($argument->getType() !== null){
            $reflection = new \ReflectionClass($argument->getType());
            if ($reflection->isSubclassOf(ApiRequest::class)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $class = $argument->getType();
        $validationRequest = new $class($this->validator, $request);
        $validationRequest->validate();
        yield $validationRequest;
    }
}
