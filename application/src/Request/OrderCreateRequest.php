<?php

namespace App\Request;

use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class OrderCreateRequest extends ApiRequest
{
    public function rules(): array
    {
        return [
            'productId' => [new NotBlank(), new NotNull()] ,
            'orderCode' => [new NotBlank(), new NotNull()],
            'quantity' => [new GreaterThan(0)],
            'address' => [new NotBlank(), new NotNull()],
            'shippingDate' => [new DateTime()],
        ];
    }
}
