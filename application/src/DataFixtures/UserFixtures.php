<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $users = [
            [
                'firstName' => "Customer 1",
                'lastName'  => "Customer 1",
                'email'     => "customer1@pathorder.com",
                'roles'     => ['ROLE_USER'],
                'password' => '123456'
            ],
            [
                'firstName' => "Customer 2",
                'lastName'  => "Customer 2",
                'email'     => "customer2@pathorder.com",
                'roles'     => ['ROLE_USER'],
                'password' => '123456'
            ],
            [
                'firstName' => "Customer 3",
                'lastName'  => "Customer 3",
                'email'     => "customer3@pathorder.com",
                'roles'     => ['ROLE_USER'],
                'password' => '123456'
            ],
        ];

        foreach ($users as $user) {
            $userEntity = new User();
            $userEntity->setFirstName($user['firstName']);
            $userEntity->setLastName($user['lastName']);
            $userEntity->setEmail($user['email']);
            $userEntity->setRoles($user['roles']);
            $userEntity->setPassword($this->passwordEncoder->encodePassword($userEntity, $user['password']));
            $manager->persist($userEntity);
        }

        $manager->flush();
    }
}
