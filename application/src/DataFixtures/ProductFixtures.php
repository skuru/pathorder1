<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Util\Assert;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $productsData = $this->getProductData();
        foreach ($productsData as $productData) {
            $product = new Product();
            $product->setName($productData['name']);
            $product->setDescription($productData['description']);
            $product->setPrice($productData['price']);
            $product->setProductCode($productData['productCode']);
            $manager->persist($product);
        }

        $manager->flush();
    }

    /**
     * @throws \Exception
     */
    private function getProductData(): array
    {
        return [
            [
                'name' => 'Product 1',
                'description' => 'Product 1 Description',
                'price' => 100,
                'productCode' => Assert::generateProductCode(),
            ],
            [
                'name' => 'Product 2',
                'description' => 'Product 2 Description',
                'price' => 2100,
                'productCode' => Assert::generateProductCode(),
            ],
            [
                'name' => 'Product 3',
                'description' => 'Product 2 Description',
                'price' => 1500,
                'productCode' => Assert::generateProductCode(),
            ]
        ];
    }
}
