<?php

namespace App\Enum;

class StatusTextType
{
    const SUCCESS = 'success';
    const ERROR = 'error';
}
