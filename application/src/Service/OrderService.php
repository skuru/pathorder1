<?php

namespace App\Service;

use App\Entity\Order;
use App\Exception\BusinessException;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class OrderService
{
    private OrderRepository $orderRepository;

    private ProductRepository $productRepository;

    private EntityManagerInterface $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     * @param OrderRepository $orderRepository
     * @param ProductRepository $productRepository
     */
    public function __construct(EntityManagerInterface $entityManager, OrderRepository $orderRepository, ProductRepository $productRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $id
     *
     * @return Order|null
     * @throws BusinessException
     */
    public function getById(int $id): ?Order
    {
        $order = $this->orderRepository->find($id);
        if ($order === null) {
            throw new BusinessException('Sipariş kaydı bulunamadı');
        }
        return $order;
    }

    /**
     * id nin durumuna göre yeni sipariş oluşturur veya var olanı getirir.
     *
     * @param int|null $id
     *
     * @return Order
     */
    public function findOrNew(?int $id): Order
    {
        $order = $id === null ? null : $this->orderRepository->find($id);
        return $order ?? new Order();
    }

    /**
     * @param int $userId
     *
     * @return int|mixed|string
     */
    public function getOrdersByAccountUser(int $userId)
    {
        return $this->orderRepository->findByUser($userId);
    }

    /**
     * Gelen sipariş bilgilerini kaydeder. Route da gelen id bilgisine göre günceller veya yeni kayıt oluşturur.
     *
     * @param Request $request
     * @param UserInterface $user
     *
     * @throws BusinessException
     */
    public function save(Request $request, UserInterface $user): void
    {
        $orderId = $request->attributes->get('id');
        $quantity = $request->get('quantity');
        $address = $request->get('address');

        $order = $this->findOrNew($orderId);

        //Eğer sipariş günü gelmiş ise düzenleme yapılamaz
        if($order->getShippingDate() !== null && Carbon::now()->gt($order->getShippingDate())){
            throw new BusinessException("Siparişiniz kargoya verildi. Güncelleme yapılamaz");
        }

        //ürün ve kullacını bilgisi ve kargo tarihi dğzenleme de değiştirilemez.
        if($order->getId() === null){
            $productId = $request->get('productId');
            $product = $this->productRepository->find($productId);

            $shippingDate = $request->get('shippingDate');
            $shippingDate = $shippingDate ? Carbon::createFromFormat('Y-m-d H:i:s', $shippingDate) : Carbon::now()->addDays(7);

            $orderCode = $request->get('orderCode');
            $order->setOrderCode($orderCode);

            $order->setProduct($product);
            $order->setUser($user);
            $order->setShippingDate($shippingDate);
        }


        //Güncelleme sırasında bu parametreleri göndermeyedebilir.
        if($quantity){
            $order->setQuantity($quantity);
        }

        if($address){
            $order->setAddress($address);
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

}
