<?php

namespace App\Controller\Api;

use App\Response\SuccessResponse;
use App\Service\UserService;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations\OpenApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route ("/customers", name="customer")
 */
class CustomerController extends AbstractController
{
    private UserService $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @Route ("", name="_list", methods={"GET"})
     *
     * @return SuccessResponse
     */
    public function list(): SuccessResponse
    {
        $users = $this->userService->getAll();

        return SuccessResponse::create()->setData($users)->setGroups('default');
    }
}
