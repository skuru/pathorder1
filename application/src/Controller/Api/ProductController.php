<?php

namespace App\Controller\Api;

use App\Response\SuccessResponse;
use App\Service\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route ("/products", name="product")
 */
class ProductController extends AbstractController
{
    private ProductService $productService;

    /**
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @Route("", name="_list", methods={"GET"})
     */
    public function index(): SuccessResponse
    {
        $products = $this->productService->getAll();
        return SuccessResponse::create()->setData($products)->setGroups(['products']);
    }
}
