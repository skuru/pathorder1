<?php

namespace App\Controller\Api;

use App\Exception\BusinessException;
use App\Request\OrderCreateRequest;
use App\Request\OrderUpdateRequest;
use App\Response\SuccessResponse;
use App\Service\OrderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route ("/orders", name="order")
 */
class OrderController extends AbstractController
{
    private OrderService $orderService;

    /**
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @Route("", name="_list", methods={"GET"})
     *
     * @return SuccessResponse
     */
    public function list(): SuccessResponse
    {
        $orders = $this->orderService->getOrdersByAccountUser($this->getUser()->getId());
        return SuccessResponse::create()->setData($orders)->setGroups('user_order');
    }

    /**
     * @Route ("/save", name="_save", methods={"POST"})
     *
     * @param OrderCreateRequest $request
     *
     * @return SuccessResponse
     * @throws BusinessException
     */
    public function save(OrderCreateRequest $request): SuccessResponse
    {
        $this->orderService->save($request->getRequest(), $this->getUser());
        return SuccessResponse::create()->setMessage("Sipariş eklendi");
    }

    /**
     * @Route ("/{id}/update", name="_update", methods={"POST"})
     *
     * @param OrderUpdateRequest $request
     *
     * @return SuccessResponse
     * @throws BusinessException
     */
    public function update(OrderUpdateRequest $request): SuccessResponse
    {
        $this->orderService->save($request->getRequest(), $this->getUser());
        return SuccessResponse::create()->setMessage("Sipariş güncellendi");
    }

    /**
     * @Route ("/{id}/detail", name="_detail", methods={"GET"})
     * @param Request $request
     *
     * @return SuccessResponse
     * @throws BusinessException
     */
    public function detail(Request $request): SuccessResponse
    {
        $orderId = $request->attributes->get('id');
        $order = $this->orderService->getById($orderId);
        return SuccessResponse::create()->setData($order)->setGroups('user_order');
    }


}
