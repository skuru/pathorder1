<?php

namespace App\Serializer\Normalizer;

use App\Response\ErrorResponse;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ErrorResponseNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    /**
     * @param mixed $object
     * @param null $format
     * @param array $context
     *
     * @return array
     */
    public function normalize($object, $format = null, array $context = []): array
    {
        $response = [
            'status'     => $object->getStatus(),
            'statusCode' => $object->getStatusCode(),
            'message'    => $object->getMessage()
        ];

        $errors = $object->getErrors();
        if (isset($errors)) {
            $response['errors'] = $errors;
        }

        return $response;
    }

    /**
     * @param mixed $data
     * @param null $format
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof ErrorResponse;
    }

    /**
     * @return bool
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
