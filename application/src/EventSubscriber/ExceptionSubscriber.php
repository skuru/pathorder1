<?php

namespace App\EventSubscriber;

use App\Exception\BusinessException;
use App\Exception\InvalidRequestException;
use App\Response\ErrorResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\SerializerInterface;

class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $errorMessage = $exception->getMessage();
        if ($exception instanceof ExpiredTokenException) {
            $response = ErrorResponse::create()->setMessage($exception->getMessage())->setStatusCode($exception->getCode());
        } elseif ($exception instanceof HttpException) {
            $response = ErrorResponse::create()->setMessage($errorMessage)->setStatusCode($exception->getStatusCode());
        } elseif ($exception instanceof InvalidRequestException) {
            $response = ErrorResponse::create()->setMessage($exception->getMessage())->setStatusCode($exception->getCode())->setErrors($exception->getErrors());
        } elseif ($exception instanceof BusinessException) {
            $response = ErrorResponse::create()->setMessage($exception->getMessage())->setStatusCode($exception->getCode());
        } else {
            $response = ErrorResponse::create()->setMessage($errorMessage)->setStatusCode(500);
        }

        $json = $this->serializer->serialize($response, 'json');

        $event->setResponse(JsonResponse::fromJsonString($json, $response->getStatusCode()));
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'kernel.exception' => 'onKernelException',
        ];
    }
}
