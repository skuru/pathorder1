<?php

namespace App\EventSubscriber;

use App\Response\ApiResponse;
use App\Response\SuccessResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class ResponseSubscriber implements EventSubscriberInterface
{
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function onKernelView(ViewEvent $event): void
    {
        $result = $event->getControllerResult();
        if ($result instanceof ApiResponse) {
            if ($result->getMessage() === null) {
                $result->setMessage('');
            }

            $context = [];
            if ($result instanceof SuccessResponse) {
                $context = [
                    AbstractObjectNormalizer::GROUPS           => $result->getGroups(),
                    AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true
                ];
            }

            $json = $this->serializer->serialize($result, 'json', $context);

            $contentLength = mb_strlen($json);
            $event->setResponse(JsonResponse::fromJsonString($json, $result->getStatusCode(), [
                'Content-Length' => $contentLength
            ]));
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ViewEvent::class => 'onKernelView',
        ];
    }
}
