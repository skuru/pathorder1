<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class RequestSubscriber implements EventSubscriberInterface
{
    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        if ($request->isMethod('POST') && $request->headers->get('Content-Type') === 'application/json') {
            $data = json_decode($request->getContent(), true);
            $request->request->replace($data);
        }

        if ($request->isMethod('POST') && (str_starts_with($request->headers->get('Content-Type'),
                'multipart/form-data'))) {
            foreach ($request->request->all() as $key => $value) {
                $request->request->set($key, json_decode($value, true));
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.request' => 'onKernelRequest',
        ];
    }
}
