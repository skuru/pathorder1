### Proje Kurulumu ###

**Kurulum Aşamaları**

Git

Proje repodan indirilir

```
git clone https://skuru@bitbucket.org/skuru/pathorder1.git
```


Docker Compose

Aşağıdaki docker-compose komutu ile geliştirme ortamı ayağa kaldırılır. Sistem projeyi otomatik çalıştıracaktır. http://localhost:8055 adresinden hizmet verecektir.
```
cd docker
docker-compose up -d --build
```

***Composer***

docker klasörünün içinde iken php servisine bağlanılır ve composer çalıştırılarak proje ayağa kaldırılır.
```
docker-compose exec php /bin/bash
```
bağlandıktan sonra 
```
composer install
```


***Veritabanı İşlemleri***

Veritabanı Migration
```
php bin/console doctrine:migrations:migrate
```

***Data Fixtures Load***

Sistem ilk çalıştırıldığında ön tanımlı verilerin veritabanına eklenmesi için aşağıdaki kod çalıştırılır. 
```
php bin/console doctrine:fixtures:load
```

***JWT Bundle Konfigurasyonu***
```
php bin/console lexik:jwt:generate-keypair --skip-if-exists
```

**Swagger Url**

Swagger entegre edildi. Bunun için NelmioApiDocBundle kullanıldı.

```
http://localhost:8055/api/doc
```

**Proje İçin Yapılanlar**

* Proje ayağa kalktığında elimizde default verilerin olması için fixture lar oluşturuldu.
* Servis katmanı oluşturuldu ve iş yıığını controller dan bu katmana aktarıldı.
* Uygulamaya özel exceptionlar oluşturuldu ve hata durumlarında dönüşün nasıl olacağı ExceptionSubscriber üzerinde belirlendi.
* Controller dönüşler için standart sağlanması için SuccessResponse, ErrorResponse classlarına taşındı. Serializer ları oluşturuldu. Responselar ResponseSubscriber ile JsonResponse a çevrildi.
* Gönderilen json datanın decode edilip request parametrelerine eklendi. Servisler hem form-data, hem json data kabul etmektedir.
* JWT kullanıldı
* Swagger eklendi.
